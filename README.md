# what's this

This repo is the source for the openshift container plattform (OKD, openshift origin) router with haproxy 2.1.

## docker link

https://hub.docker.com/r/me2digital/okd-router-hap21

# run this image

## check the current image
```
oc -n default get dc/router \
  -o jsonpath='{.spec.template.spec.containers[0].image}{"\n"}'
```

## Patch the new image
```
oc -n default patch dc/router \
  --type='json' \
  -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/image","value":"docker.io/me2digital/okd-router-hap21:latest"}]'
```

After the patch will the router be deployed.

You can now enable the haproxy-enhancements as described in the doc.
https://docs.openshift.com/container-platform/3.11/release_notes/ocp_3_11_release_notes.html#ocp-311-haproxy-enhancements

The backend communication (server line) have "alpn h2,http/1.1" therefore will haproxy try to talk with the backend with HTTP/2 
when the backend offers this protocol.

## use cookie attribute SameSite=Strict

```
oc -n <PROJECT/NS> annotate route <YOUR-ROUTE> haproxy.router.openshift.io/cookie_same_site=true
```

# how to build

## Docker build
```
git clone https://gitlab.com/aleks001/okd-router-hap21.git
cd okd-router-hap21.git
docker build --rm --tag me2digital/okd-router-hap21:latest .
```

## on openshift
```
oc new-app https://gitlab.com/aleks001/okd-router-hap21.git
oc logs -f bc/okd-router-hap21
```

wait until the Build is successfully.

```
oc  -n default logs -f router-3-5prbb
I0411 03:58:00.766102       1 template.go:297] Starting template router (v3.11.0+39132cb-398)
I0411 03:58:00.768882       1 metrics.go:147] Router health and metrics port listening at 0.0.0.0:1936 on HTTP and HTTPS
E0411 03:58:00.775065       1 haproxy.go:392] can't scrape HAProxy: dial unix /var/lib/haproxy/run/haproxy.sock: connect: no such file or directory
I0411 03:58:00.803313       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).
I0411 03:58:00.803395       1 router.go:252] Router is including routes in all namespaces
I0411 03:58:01.053602       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).
I0411 03:58:06.045254       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).
I0411 03:58:11.517210       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).

```

# various output

## ps

```
oc -n default rsh router-3-5prbb
sh-4.2$ ps axf
   PID TTY      STAT   TIME COMMAND
    62 ?        Ss     0:00 /bin/sh
    69 ?        R+     0:00  \_ ps axf
     1 ?        Ssl    0:01 /usr/bin/openshift-router
    45 ?        Ss     0:00 /usr/sbin/haproxy -f /var/lib/haproxy/conf/haproxy.config -p /var/lib/haproxy/run/haproxy.pid -x /var/lib/haproxy/r
```

## version

```
oc -n default rsh router-3-5prbb /usr/bin/openshift-router version
openshift-router v3.11.0+39132cb-398
```

## haproxy -vv 
```
oc rsh router-7-qzdvt haproxy -vv

HA-Proxy version 2.1.4 2020/04/02 - https://haproxy.org/
Status: stable branch - will stop receiving fixes around Q1 2021.
Known bugs: http://www.haproxy.org/bugs/bugs-2.1.4.html
Build options :
  TARGET  = linux-glibc
  CPU     = generic
  CC      = gcc
  CFLAGS  = -O2 -g -fno-strict-aliasing -Wdeclaration-after-statement -fwrapv -Wno-unused-label -Wno-sign-compare -Wno-unused-parameter -Wno-old-style-declaration -Wno-ignored-qualifiers -Wno-clobbered -Wno-missing-field-initializers -Wtype-limits
  OPTIONS = USE_PCRE=1 USE_PCRE_JIT=1 USE_OPENSSL=1 USE_LUA=1 USE_ZLIB=1

Feature list : +EPOLL -KQUEUE -MY_EPOLL -MY_SPLICE +NETFILTER +PCRE +PCRE_JIT -PCRE2 -PCRE2_JIT +POLL -PRIVATE_CACHE +THREAD -PTHREAD_PSHARED -REGPARM -STATIC_PCRE -STATIC_PCRE2 +TPROXY +LINUX_TPROXY +LINUX_SPLICE +LIBCRYPT +CRYPT_H -VSYSCALL +GETADDRINFO +OPENSSL +LUA +FUTEX +ACCEPT4 -MY_ACCEPT4 +ZLIB -SLZ +CPU_AFFINITY +TFO +NS +DL +RT -DEVICEATLAS -51DEGREES -WURFL -SYSTEMD -OBSOLETE_LINKER +PRCTL +THREAD_DUMP -EVPORTS

Default settings :
  bufsize = 16384, maxrewrite = 1024, maxpollevents = 200

Built with multi-threading support (MAX_THREADS=64, default=4).
Built with OpenSSL version : OpenSSL 1.1.1f  31 Mar 2020
Running on OpenSSL version : OpenSSL 1.1.1f  31 Mar 2020
OpenSSL library supports TLS extensions : yes
OpenSSL library supports SNI : yes
OpenSSL library supports : TLSv1.0 TLSv1.1 TLSv1.2 TLSv1.3
Built with Lua version : Lua 5.3.5
Built with network namespace support.
Built with transparent proxy support using: IP_TRANSPARENT IPV6_TRANSPARENT IP_FREEBIND
Built with PCRE version : 8.32 2012-11-30
Running on PCRE version : 8.32 2012-11-30
PCRE library supports JIT : yes
Encrypted password support via crypt(3): yes
Built with zlib version : 1.2.7
Running on zlib version : 1.2.7
Compression algorithms supported : identity("identity"), deflate("deflate"), raw-deflate("deflate"), gzip("gzip")
Built with the Prometheus exporter as a service

Available polling systems :
      epoll : pref=300,  test result OK
       poll : pref=200,  test result OK
     select : pref=150,  test result OK
Total: 3 (3 usable), will use epoll.

Available multiplexer protocols :
(protocols marked as <default> cannot be specified using 'proto' keyword)
              h2 : mode=HTTP       side=FE|BE     mux=H2
            fcgi : mode=HTTP       side=BE        mux=FCGI
       <default> : mode=HTTP       side=FE|BE     mux=H1
       <default> : mode=TCP        side=FE|BE     mux=PASS

Available services :
	prometheus-exporter

Available filters :
	[SPOE] spoe
	[CACHE] cache
	[FCGI] fcgi-app
	[TRACE] trace
	[COMP] compression
```
